#!/bin/bash

set -e

if [ "$EUID" -eq 0 ]; then
	>&2 echo [-] Running this script with root privileges is not recommended
	exit
fi

if [ "$0" != "$BASH_SOURCE" ]; then
	>&2 echo [-] Do not sourcing the script
	return
fi

while [[ $PWD != '/' && ! -d $PWD/.repo ]]; do cd ..; done
if [[ $PWD == '/' ]]; then
	>&2 echo [-] Please ensure you are running this script within the Yocto SDK directory
	exit
fi

# EULA
if [ -f .eula_accepted ]; then
	ACCEPT_EULA=1
else
	ACCEPT_EULA=0
fi

OPTIND=1

while getopts "e" opt; do
	case "$opt" in
		e)
			ACCEPT_EULA=1
			;;
	esac
done

shift $((OPTIND-1))

if [ $ACCEPT_EULA -eq 0 ];then
	TERMINAL_SIZE=($(stty -a | tr ';' '\n' | grep -E "rows|columns" | cut -d ' ' -f3))
	TERMINAL_SIZE[0]=$((TERMINAL_SIZE[0] -= 4))
	TERMINAL_SIZE[1]=$((TERMINAL_SIZE[1] -= 4))
	TERMINAL_SIZE=${TERMINAL_SIZE[@]}

	if (whiptail \
		--title "End User License Agreement" \
		--textbox "./tools/EULA.txt" \
		--scrolltext \
		$TERMINAL_SIZE \
		--ok-button "Proceed to confirm" \
		); then
		if (whiptail \
			--title "End User License Agreement" \
			--yesno "Please confirm to use the TOPST SDK" 10 50 \
			--yes-button Accept \
			--no-button Reject\
			); then
			ACCEPT_EULA=1
		else
			>&2 echo [-] EULA rejected
			exit
		fi
	else
		>&2 echo [-] EULA rejected
		exit
	fi

	unset TERMINAL_SIZE

	touch .eula_accepted
fi

mkdir -p downloads

# Main core
INIT_MAINCORE=0
function prepare_build_main() {
	USE_RESTRICTED=0

	# Enable restricted features
	if [ -d yocto/meta-topst-restricted ]; then
		if (whiptail \
			--title "Warning: restricted package detected" \
			--yesno "Please confirm to use the restricted package" 10 50 \
			--yes-button Accept \
			--no-button Reject\
			); then
			USE_RESTRICTED=1
		else
			USE_RESTRICTED=0
		fi
	fi

	mkdir -p build-main/conf

	cp yocto/meta-topst/conf/bblayers.conf.sample build-main/conf/bblayers.conf
	cp yocto/meta-topst/conf/local.conf.sample build-main/conf/local.conf
	echo -n "../meta-topst/conf" > build-main/conf/templateconf.cfg

	if [ $USE_RESTRICTED -eq 1 ]; then
		sed -i -E '/(.+meta-topst)/a \  ##OEROOT##/meta-topst-restricted \\' \
			build-main/conf/bblayers.conf
		sed -i 's/^VIDEO_SINK .*/INVITE_PLATFORM += "support-4k-video"/' \
			build-main/conf/local.conf
		sed -i 's/^DISTRO .*/DISTRO ?= "poky-topst-systemd-restricted"/' \
			build-main/conf/local.conf
	fi

	sed -i "s,##OEROOT##,$PWD/yocto,g" build-main/conf/bblayers.conf
	sed -i "s,^#DL_DIR ?= .*,DL_DIR = \"$PWD/downloads\",g" build-main/conf/local.conf

	cat << EOL
[+] Please execute the following command to initiate the build process

	source yocto/poky/oe-init-build-env build-main
	bitbake topst

EOL
}

# Sub core
INIT_SUBCORE=0
WITH_SUBCORE=0
function prepare_build_sub() {
	mkdir -p build-sub/conf

	cp yocto/meta-topst-subcore/conf/bblayers.conf.sample build-sub/conf/bblayers.conf
	cp yocto/meta-topst-subcore/conf/local.conf.sample build-sub/conf/local.conf
	echo -n "../meta-topst-subcore/conf" > build-sub/conf/templateconf.cfg

	sed -i "s,##OEROOT##,$PWD/yocto,g" build-sub/conf/bblayers.conf
	sed -i "s,^#DL_DIR ?= .*,DL_DIR = \"$PWD/downloads\",g" build-sub/conf/local.conf

	cat << EOL
[+] Please execute the following command to initiate the build process for subcore

	source yocto/poky/oe-init-build-env build-sub
	bitbake topst-subcore

EOL
}

# patch local.conf to enable/disable subcore
function patch_subcore_option_in_local_conf() {
	CONF="build-main/conf/local.conf"
	CHANGED=0

	if [ "$1" == "enable" ]; then
		if grep -q '^#INVITE_PLATFORM.*with-subcore' $CONF; then
			CHANGED=1
			sed -i 's/^#\(INVITE_PLATFORM.*with-subcore\)/\1/' $CONF
		fi
	else
		if grep -q '^INVITE_PLATFORM.*with-subcore' $CONF; then
			CHANGED=1
			sed -i 's/^\(INVITE_PLATFORM.*with-subcore\)/#\1/' $CONF
		fi
	fi

	if [ $CHANGED -eq 1 ]; then
		echo [!] Rebuild main core image to reflect changes
	fi
}

if [ -d build-main/conf ]; then
	echo "[!] Existing build-main directories have been detected"
	read -p "    Do you want to delete? (yes/No) " yn
	case $yn in
		[yY]* )
			echo -n "[!] Remove existing build directories - "
			rm -rf build-main
			echo Done
			INIT_MAINCORE=1
			;;
		""|[nN]* )
			;;
		* )
			echo [-] Invalid response.
			exit 1;;
	esac
else
	INIT_MAINCORE=1
fi

# populate build-main
if [ $INIT_MAINCORE -eq 1 ]; then
	prepare_build_main
fi

while true; do
	read -p "[!] Will you use subcore? (yes/no) " yn
	case $yn in
		[Yy]* )
			WITH_SUBCORE=1
			break;;
		[Nn]* )
			patch_subcore_option_in_local_conf disable
			exit
			;;
		* ) ;;
	esac
done

if [ -d build-sub/conf ]; then
	echo "[!] Existing build-sub directories have been detected"
	read -p "    Do you want to delete? (yes/No) " yn
	case $yn in
		[yY]* )
			echo -n "[!] Remove existing build directories - "
			rm -rf build-sub
			echo Done
			INIT_SUBCORE=1
			;;
		""|[nN]* )
			;;
		* )
			echo [-] Invalid response.
			exit 1;;
	esac
else
	INIT_SUBCORE=1
fi

# populate build-sub
if [ $INIT_SUBCORE -eq 1 ]; then
	prepare_build_sub
fi

if [ $WITH_SUBCORE -eq 1 ]; then
	patch_subcore_option_in_local_conf enable
else
	patch_subcore_option_in_local_conf disable
fi

: <<'END'
[Main Core]
source yocto/poky/oe-init-build-env build-main
bitbake topst

[Sub Core]
source yocto/poky/oe-init-build-env build-sub
bitbake topst-subcore
END
